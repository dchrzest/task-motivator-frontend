import React, { Component } from "react";
import TasksActiveAndDoneTabs from "./Components/SimpleTask/TasksActiveAndDoneTabs";
import currentUserService from "./Services/CurrentUserService";

export default class MainUserView extends Component {
  constructor(props) {
    super();
    this.state = {
      Email: "",
      Name: "",
      Surname: "",
      AuthenticationType: ""
    };
  }

  componentWillMount() {
    currentUserService.getUserData(userData => {
      console.log(userData);
      this.setState({
        Email: userData.email,
        AuthenticationType: userData.authenticationType,
        Name: userData.name,
        Surname: userData.surname
      });
    });
  }

  render() {
    return (
      <div>
        <h2>
          {this.state.Name} {this.state.Surname}
        </h2>
        <TasksActiveAndDoneTabs />
      </div>
    );
  }
}
