import configuration from "../Configuration";
import axios from "axios";
import accountService from "./AccountService";

const simpleTaskActionsService = {
  setTaskDone: function(taskData, onSuccessCallback) {
    axios
      .post(
        configuration.apiUrl + "simpletasks/SetTaskDone",
        taskData,
        accountService.getJWTHeader()
      )
      .then(res => {
        onSuccessCallback();
      })
      .catch(error => {
        console.log(error);
      });
  }
};
export default simpleTaskActionsService;
