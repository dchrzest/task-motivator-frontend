import axios from "axios";
import configuration from "../Configuration";
import accountService from "./AccountService";

const simpleTaskService = {
  addTask: function(taskData, onSuccessCallback) {
    axios
      .post(
        configuration.apiUrl + "simpletasks/add",
        taskData,
        accountService.getJWTHeader()
      )
      .then(res => {
        if (onSuccessCallback) {
          console.log(JSON.stringify(res));
          onSuccessCallback();
        }
      })
      .catch(error => {
        console.log(JSON.stringify(error));
      });
  },

  getUserTasks: function(onSuccessCallback) {
    axios
      .get(configuration.apiUrl + "simpletasks/get", accountService.getJWTHeader())
      .then(res => {
        if (onSuccessCallback) onSuccessCallback(res.data);
      })
      .catch(error => {
        console.log(JSON.stringify(error));
      });
  },
  getCurrentTasks:function(onSuccessCallback){
    axios.get(configuration.apiUrl + "simpletasks/GetCurrentTasks", accountService.getJWTHeader())
    .then(res => {
      if (onSuccessCallback) onSuccessCallback(res.data);
    })
    .catch(error => {
      console.log(error);
    });
  },
  getDoneTasks:function(onSuccessCallback){
    axios.get(configuration.apiUrl + "simpletasks/getDoneTasks", accountService.getJWTHeader())
    .then(res => {
      if (onSuccessCallback) onSuccessCallback(res.data);
    })
    .catch(error => {
      console.log(error);
    });
  },
  getFullHistory:function(onSuccessCallback){
    axios.get(configuration.apiUrl + "simpletasks/GetFullHistoryOfDoneTasks", accountService.getJWTHeader())
    .then(res => {
      if (onSuccessCallback) onSuccessCallback(res.data);
    })
    .catch(error => {
      console.log(JSON.stringify(error));
    });
  },

  getTask:function(taskId, onSuccessCallback){
    axios.get(configuration.apiUrl + "simpletasks/getTask?Id=" + taskId, accountService.getJWTHeader())
    .then(res => {
      if (onSuccessCallback) onSuccessCallback(res.data);
    })
    .catch(error => {
      console.log(JSON.stringify(error));
    });
  },

  getTasksToCheck:function(onSuccessCallback){
    axios.get(configuration.apiUrl + "simpletasks/GetTasksToCheck", accountService.getJWTHeader())
    .then(res => {
      if (onSuccessCallback) onSuccessCallback(res.data);
    })
    .catch(error => {
      console.log(JSON.stringify(error));
    });
  },

  editTask: function(taskData, onSuccessCallback) {
    axios
      .put(
        configuration.apiUrl + "simpletasks",
        taskData,
        accountService.getJWTHeader()
      )
      .then(res => {
        if (onSuccessCallback) onSuccessCallback();
      })
      .catch(error => {
        console.log(JSON.stringify(error));
      });
  },

  deleteTask: function(taskId, onSuccessCallback) {
    axios
      .delete(
        configuration.apiUrl + "simpletasks/delete?Id=" + taskId,
        accountService.getJWTHeader()
      )
      .then(res => {
        if (onSuccessCallback) onSuccessCallback();
      })
      .catch(error => {
        console.log(JSON.stringify(error));
      });
  }
};
export default simpleTaskService;
