import axios from "axios";
import configuration from "../Configuration";
import accountService from "./AccountService";

const currentUserService = {
  getUserData: function(callbackMethod) {
    axios
      .get(configuration.apiUrl + "CurrentUser/GetUserData", accountService.getJWTHeader())
      .then(res => {
        console.log(res);
        if (callbackMethod) {
          callbackMethod(res.data);
        }
        // window.loctaion.reload();
      })
      .catch(error => {
        console.log(error);
        if (callbackMethod) {
          callbackMethod(error);
        }
      });
  }
};

export default currentUserService;
