import axios from 'axios';
import configuration from '../Configuration';

const accountService = {

    //funkcja odpowiedzialna za logowanie użytkownika
    login: function (email, password, callbackMethod) {
        if (email != null && password != null) {
            if (localStorage.getItem('email') && localStorage.getItem('jwt'))
                return;

            axios.post(configuration.apiUrl + "Account/Login", { email, password })
                .then(res => {
                    console.log(res);
                    if(callbackMethod) {
                        callbackMethod(res.status)
                    }
                    localStorage.setItem('email', email);
                    this.setJWT(res.data);

                    window.location.reload();
                })
                .catch((error) => {
                    console.log(error)
                    if(callbackMethod) {
                        callbackMethod(error)
                    }
                });
        }
    },

    //zwraca token JWT lub null
    getJWT: function () {
        return localStorage.getItem("jwt");
    },

    //ustawia token JWT
    setJWT: function (jwt) {
        if (jwt != null)
            localStorage.setItem("jwt", jwt)
    },

    //zwraca header z JWT potrzebny do wykonywania zapytań do API
    getJWTHeader: function () {
        var config = {
            headers: {
                Authorization: "Bearer " + this.getJWT()
            }
        }

        return config;
    },

    //wylogowywuje użytkownika, usuwa JWT
    logout: function () {
        localStorage.clear();
    },

    //sprawdza czy użytkownik jest zalogowany, czy został zapisany jego JWT
    isLogged: function () {
        if (this.getJWT() != null) {
            return true;
        } else {
            return false;
        }
    },

    changePassword: function (oldPassword, newPassword, confirmNewPassword, callbackMethod) {
        if (oldPassword != null && newPassword != null && confirmNewPassword != null) {
            axios.post(configuration.apiUrl + "AccountManager/ChangePassword", { oldPassword, newPassword, confirmNewPassword }, this.getJWTHeader())
                .then(res => {
                    console.log(res);
                    if(callbackMethod) {
                        callbackMethod(res.status);
                    }
                    localStorage.setItem("Debug", res)
                })
                .catch((error) => {
                    console.log(error)
                    if(callbackMethod) {
                        callbackMethod(error.response.status);
                    }
                });

            console.log("ChangePasswordService, changePassword function");
            console.log("Link: " + configuration.apiUrl + "AccountManager/ChangePassword");
        }
    },
    deleteAccount: function (password, callbackMethod) {

        if (password != null) {
            axios.post(configuration.apiUrl + "AccountManager/DeleteAccount", { password }, this.getJWTHeader())
                .then(res => {
                    console.log(res);
                    if(callbackMethod) {
                        callbackMethod(res.status);
                    }
                    localStorage.setItem("Debug", res)
                    localStorage.setItem("Debug", res)
                    if(res.status === 200) {
                        accountService.logout();
                        window.location.reload();
                    }
                })
                .catch((error) => {
                    console.log(error)
                    if(callbackMethod) {
                        callbackMethod(error.response.status);
                    }
                });

            console.log("DeleteAccountService, deleteAccount function");
            console.log("Link: " + configuration.apiUrl + "AccountManager/DeleteAccount");
            // accountService.logout();
        }
    },
    changeEmail: function (newEmail, callbackMethod) {

        if (newEmail != null) {

            axios.post(configuration.apiUrl + "AccountManager/ChangeEmail", { newEmail }, this.getJWTHeader())
                .then(res => {
                    console.log(res);
                    if(callbackMethod) {
                        callbackMethod(res.status);
                    }
                    localStorage.setItem("Debug", res)
                    localStorage.setItem("email", newEmail)
                    if(res.status === 200) {
                        window.location.reload();
                    }
                })
                .catch((error) => {
                    console.log(error)
                    if(callbackMethod) {
                        callbackMethod(error.response.status);
                    }
                });

            console.log("ChangeEmailService, changeEmail function");
            console.log("Link: " + configuration.apiUrl + "AccountManager/ChangeEmail");
        }
    },

    register: function (name, surname, email, password, callbackMethod) {

        if (email != null && password != null) 
        {
            //{email, password} is equal {email: email, password: password}
            axios.post(configuration.apiUrl + "Account/Register", { name, surname, email, password })
                .then(res => {
                    console.log(res);
                    localStorage.setItem("Debug", res);
                    localStorage.setItem('email', email);
                    this.setJWT(res.data);

                    if(callbackMethod) {
                        callbackMethod(res.status)
                    }

                    window.location.reload();
                })
                .catch((error) => {
                    console.log(error)
                    if(callbackMethod) {
                        callbackMethod(error.response.status);
                    }
                });
            console.log("RegistrationService, register function");
        }
    }
}

export default accountService;