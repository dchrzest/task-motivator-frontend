import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import ManageTasks from "./ManageTasks";
import History from "./History";
import CreateSimpleTask from "./Components/SimpleTask/CreateSimpleTask";
import Calendar from "./Calendar";
import Login from "./Components/User/Login";
import Register from "./Components/User/Register";
import StartPage from "./StartPage";
import accountService from "./Services/AccountService";
import Logout from "./Components/User/Logout";
import BodyComponent from "./Components/BodyComponent";
import MainUserView from "./MainUserView";
import EditSimpleTask from "./Components/SimpleTask/EditSimpleTask";

const PrivateRoute = ({ component: Component, ...rest }) => (
  //TODO redirect do referera
  <Route
    {...rest}
    render={props =>
      accountService.isLogged() === true ? (
        <Component {...props} />
      ) : (
        <Redirect to="/StartPage" />
      )
    }
  />
);

const UnloggedRoute = ({ component: Component, ...rest }) => (
  //TODO redirect do referera
  <Route
    {...rest}
    render={props =>
      accountService.isLogged() === false ? (
        <Component {...props} />
      ) : (
        <Redirect to="/" />
      )
    }
  />
);

export default () => (
  <Switch>
    <UnloggedRoute
      path="/StartPage"
      component={() => <BodyComponent body={StartPage} />}
    />
    <UnloggedRoute path="/logout" component={Logout} />
    <PrivateRoute
      exact
      path="/"
      component={() => <BodyComponent body={MainUserView} />}
    />
    <PrivateRoute
      exact
      path="/ManageTasks"
      component={() => <BodyComponent body={ManageTasks} />}
    />
    <PrivateRoute
      path="/History"
      component={() => <BodyComponent body={History} />}
    />
    <PrivateRoute
      path="/CreateSimpleTask"
      component={() => <BodyComponent body={CreateSimpleTask} />}
    />
    <PrivateRoute
      path="/Calendar"
      component={() => <BodyComponent body={Calendar} />}
    />
    {/* <Route
        path={`/Edit/:Id`}
      
        render={props =>
          accountService.isLogged() === true ? (
            <EditSimpleTask Id={props.match.params.Id} {...props} />
          ) : (
            <Redirect to="/StartPage" />
          )
        }
      /> */}
  </Switch>
);
