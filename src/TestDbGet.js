import React, { Component } from "react";
import axios from "axios";
import accountService from "./Services/AccountService";

const header = {
  headers: {
    Authorization:
      "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ3aXNwZXJlazJAd3AucGwiLCJqdGkiOiIxZjZmYTEyZi1hMDcxLTQ5NmYtYWZkZS03MWNiN2E4MWRkYzAiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE1MDI1ODMzLTE4MzktNDc1Zi05ZTk0LThlOTBkYzRkNTBhNyIsImV4cCI6MTU1OTczMzM4NiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIn0.t5UgHPREBdY1y9sd6kPQ6z3TlFJN1Kvnqk6J61lNlD4"
  }
};

export default class TestDbGet extends Component {
  state = {
    getResponse: {}
  };

  componentDidMount() {

  console.log("JWT: " +     accountService.getJWT());

    axios.get("https://localhost:44314/api/simpletasks", header).then(response => {
      console.log(response);
      this.setState({
        getResponse: response
      });
    });
  }

  render() {
    return (
      <div>
        <p>GetResponse:</p>
        {JSON.stringify(this.state.getResponse)}
      </div>
    );
  }
}

