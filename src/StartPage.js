import Register from "./Components/User/Register";
import React from "react";
import Login from "./Components/User/Login";
const StartPage = props => {
  return (
    <div style={{ display: "-webkit-flex" }}>
      <div style={{margin: 10,}}>
        <Login />
      </div>
      <div style={{margin: 10,}} >
        <Register />
      </div>
    </div>
  );
};
export default StartPage;
