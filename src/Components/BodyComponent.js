import React, { Component } from "react";
import accountService from "../Services/AccountService";
import MainNavbar from "./MainView/MainNavbar";
import StartPageNavbar from "./MainView/StartPageNavbar";

class BodyComponent extends Component {
  render() {
    return (
      <div>
        <div>
          {accountService.isLogged() ? <MainNavbar /> : <StartPageNavbar />}
        </div>

        <div className="container" style={{marginTop:10}}>
          <div className="row">
            <div className="col-lg-12 ">{<this.props.body />}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default BodyComponent;
