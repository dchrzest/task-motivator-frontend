import React, { Component } from 'react';
import accountService from '../../Services/AccountService';
import {
    Redirect
  } from 'react-router-dom';
import AlertPopUpComponent from '../Additionals/AlertPopUpComponent';

class Logout extends Component {

    componentDidMount() {
        debugger;
        if(localStorage.getItem('jwt')){
            accountService.logout();  
            this.callbackMethod(200);
            console.log("The user has been logged out.");   
        }
    }

    callbackMethod() {
        return(
            <AlertPopUpComponent message="Log out successful!"></AlertPopUpComponent>
        )
    }

    render() {
        debugger;
        return (
            <div>
                <Redirect to="/" />
            </div>
        );
    }
}

export default Logout;