import React, { Component } from "react";
import { Button, Form, FormGroup, Label, Input, FormText } from "reactstrap";
import accountService from "../../Services/AccountService";
import { ServerURL } from "../../ServerURL";

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      surname: "",
      email: "",
      password: "",
      repeatPassword: ""
    };
  }

  handleSubmit = event => {
    event.preventDefault();
    const { name, surname, email, password, repeatPassword } = this.state;
    if (password === repeatPassword) {
      accountService.register(name, surname, email, password);
    }
  };
  registerUrl = ServerURL() + "register";

  handleChange = async event => {
    const { target } = event;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const { name } = target;
    await this.setState({
      [name]: value
    });
  };

  render() {
    const { name, surname, email, password, repeatPassword } = this.state;
    return (
      <div>
        <h2>Register</h2>
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label for="exampleName">Name</Label>
            <Input
              type="text"
              name="name"
              id="name"
              value={name}
              placeholder="name"
              onChange={e => {
                this.handleChange(e);
              }}
            />
          </FormGroup>
          <FormGroup>
            <Label for="exampleSurname">Surname</Label>
            <Input
              type="text"
              name="surname"
              id="surname"
              value={surname}
              placeholder="surname"
              onChange={e => {
                this.handleChange(e);
              }}
            />
          </FormGroup>
          <FormGroup>
            <Label for="exampleEmail">Email</Label>
            <Input
              type="email"
              name="email"
              id="email"
              value={email}
              placeholder="email"
              onChange={e => {
                this.handleChange(e);
              }}
            />
          </FormGroup>
          <FormGroup>
            <Label for="examplePassword">Password</Label>
            <Input
              type="password"
              name="password"
              id="password"
              value={password}
              placeholder="password"
              onChange={e => {
                this.handleChange(e);
              }}
            />
          </FormGroup>
          <FormGroup>
            <Label for="examplePassword">Repeat Password</Label>
            <Input
              type="password"
              name="repeatPassword"
              value={repeatPassword}
              id="repeatPassword"
              placeholder="password"
              onChange={e => {
                this.handleChange(e);
              }}
            />
          </FormGroup>
          <Button variant="primary" type="submit">
            Register
          </Button>
        </Form>
      </div>
    );
  }
}
