import React, { Component } from "react";
import { Button, Form, FormGroup, Label, Input, FormText } from "reactstrap";
import accountService from "../../Services/AccountService";
import { ServerURL } from "../../ServerURL";
import {Redirect} from 'react-router-dom'
export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }

  loginResponse = (response) =>{
    if(response.status === 200){
      return <Redirect to='/login'  />
    }
  };

  handleSubmit = event => {
    event.preventDefault();
    const { email, password } = this.state;
    accountService.login(email, password, this.loginResponse);
  };

  handleChange = async event => {
    const { target } = event;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const { name } = target;
    await this.setState({
      [name]: value
    });
  };

  render() {
    const { email, password } = this.state;
    return (
      <div>
        <h2>Login</h2>
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label for="exampleEmail">Email</Label>
            <Input
              type="email"
              name="email"
              id="exampleEmail"
              value={email}
              placeholder="email"
              onChange={e => {
                this.handleChange(e);
              }}
            />
          </FormGroup>
          <FormGroup>
            <Label for="examplePassword">Password</Label>
            <Input
              type="password"
              name="password"
              id="password"
              value={password}
              placeholder="password`"
              onChange={e => {
                this.handleChange(e);
              }}
            />
          </FormGroup>
          <Button variant="primary" type="submit">
            Login
          </Button>
        </Form>
      </div>
    );
  }
}
