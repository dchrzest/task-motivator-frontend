import React, { Component } from "react";
import TimeSpan from "./TimeSpan";

const TimeLeft = (props) => {

    console.log(props);
    let endDate = new Date(props.end).getTime();
    var substracted = endDate - props.start;
    var timeSpanScript = new TimeSpan(substracted);
   
    let displayText = "";
    if(timeSpanScript.days() > 0){
        displayText += timeSpanScript.days() + " days ";
    } 
    if( timeSpanScript.hours() > 0 )
    {
        displayText += timeSpanScript.hours() + " hours ";
    }
    displayText += timeSpanScript.minutes() + " minutes left";
    return(
        <div>
            {displayText}
        </div>
    );


}
export default TimeLeft;