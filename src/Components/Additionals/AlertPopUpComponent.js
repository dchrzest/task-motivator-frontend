import React, { Component } from 'react';

class AlertPopUpComponent extends Component {
    constructor() {
        super();

        this.state = {
        };
    }

    render() {
        return (
            <div className="alert alert-danger alert-dismissible fade show" role="alert">{this.props.message}</div>
        );
    }
}

export default AlertPopUpComponent;