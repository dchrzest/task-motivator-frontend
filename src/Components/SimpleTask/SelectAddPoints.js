import Select from "react-select";
import React, { Component } from 'react';

const taskLevel = [
  { value: "1", label: "Not even a task(1)" },
  { value: "2", label: "Five Seconds Task(2)" },
  { value: "3", label: "One Minute Task(3)" },
  { value: "4", label: "Light Task(4)" },
  { value: "5", label: "Task(5)" },
  { value: "6", label: "Big Task(6)" },
  { value: "7", label: "Hard Task(7)" },
  { value: "8", label: "Mega Hard task(8)" },
  { value: "9", label: "Ultra Mega Hard Task(9)" },
  { value: "10", label: "Hardcore Ultra Mega Hard Task(10)" }
];

export default class SelectAddPoints extends Component {
  constructor(...args) {
    super(...args);

    this.state = {
      pointsOption:0
    };
  }

  onChange = (option) => {
    console.log("option: " +  JSON.stringify( option))
    this.setState({
      pointsOption : option.value
    });
    this.props.onChange(option.value)
  }

  render() {
    return (
      <div>
 
        <Select
          onChange={this.onChange}
          options={taskLevel}
          />
      </div>

    )
  }
}
