import React, { Component } from "react";
import Select from "react-select";
import { debug } from "util";
const options = [
  { value: "1", label: "Monday" },
  { value: "2", label: "Tuesday" },
  { value: "3", label: "Wednesday" },
  { value: "4", label: "Thursday" },
  { value: "5", label: "Friday" },
  { value: "6", label: "Saturday" },
  { value: "7", label: "Sunday" }
];
export default class SelectDayInWeek extends Component {
  constructor(...args) {
    super(...args);

    this.state = {
      daySelectedOption: {}
    };
  }

  handleDayInWeekChange = selectedOption => {
    debugger;
    console.log("Todays day: "+  new Date().getDay());
    var date2 = new Date().getDate() +  (selectedOption.value - (new Date().getDay()) % 7);
    var todayDay = new Date().getDay();
    var addedDays =(parseInt(selectedOption.value) -todayDay  ) % 7;
   console.log("Added days: " + addedDays);
    var date = new Date().setDate(new Date().getDate() +  addedDays);
     var date3 = new Date(date);
    console.log("Wanted day: "+  date);
    console.log("Wanted day2: "+  date2);
    console.log("Wanted day3: "+  date3);
    console.log("SelectedOption " + JSON.stringify(selectedOption));
    
    this.setState({
      daySelectedOption: selectedOption
    });
  };

  render() {
   return(
        <Select
        value={this.state.daySelectedOption}
        onChange={this.handleDayInWeekChange}
        options={options}
    />
   );
  }
}
