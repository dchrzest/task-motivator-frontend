import React, { Component } from "react";
import DatePicker from "react-datepicker";
import setMinutes from "date-fns/set_minutes";
import setHours from "date-fns/set_hours";
import "react-datepicker/dist/react-datepicker.css";

export default class DatePickerDayAndTime extends Component {
  constructor(...args) {
    super(...args);

    this.state = {
      startDate: new Date()
    };
  }

  componentDidMount(){
    this.updateProps(this.state.startDate);
  }

  updateProps(value){
    var date = new Date(value);
    date.setMinutes(Math.round(date.getMinutes()/30)*30)
    date.setSeconds(0);
    date.setMilliseconds(0);
    console.log("dateinpicker: " + JSON.stringify(date));
    this.setState({
      startDate: date
    });
    this.props.onChange(date);
  };

  handleChange = value => {
    this.updateProps(value);
  };
  render() {
    return (
      <DatePicker
        selected={this.state.startDate}
        onChange={this.handleChange}
        showTimeSelect
        minDate={new Date()}
        timeFormat="HH:mm"
        dateFormat="MMMM d, yyyy HH:mm"
      />
    );
  }
}
