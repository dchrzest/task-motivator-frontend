import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
export default class DatePickerEveryday extends Component {
  constructor(...args) {
    super(...args);

    this.state = {
      startTime: new Date(),
      everydayTime : new Date()
    };
  }
  handleEveryDayChange = value => {
    console.log("EveryDayPickerValue: " + JSON.stringify(value));
    this.setState({
      everydayTime: value
    });
  };

  render() {
    return (
      <DatePicker
        selected={this.state.everydayTime}
        onChange={this.handleEveryDayChange}
        showTimeSelect
        showTimeSelectOnly
        timeIntervals={15}
        dateFormat="h:mm aa"
        timeCaption="Time"
      />
    );
  }
}
