import React, { Component } from "react";
import { Button, ListGroup, Dropdown,ListGroupItem, FormGroup  } from "reactstrap";
import { TimePicker } from "react-time-picker";

import SelectDayInWeek from "./SelectDayInWeek";
import DatePickerEveryday from "./DatePickerEveryday";
import DatePickerDayAndTime from "./DatePickerDayAndTime";

export default class RepeatingPeriod extends Component {
  constructor(...args) {
    super(...args);

    this.state = {
      repeatPeriod: "EveryDay",
      selectedTime: new Date()
    };
  }

  addRepeatingPeriod = () => {
    console.log(this.state.repeatPeriod);
    console.log(this.state.selectedTime);
    this.props.onSubmit({
      repeatPeriod: this.state.repeatPeriod,
      startTime: this.state.selectedTime
    });
  };

  setRepeatPeriod = event => {
    
    this.setState({
      repeatPeriod: event.target.value
    });
    console.log(event.target.value);
  };

  onTimeChange = value => {
    console.log("Selected time: " + value);
    this.setState({
      selectedTime: value
    });
  };

  render() {
    return (
      <div>
        <ListGroup onChange={this.setRepeatPeriod.bind(this)}>
          <ListGroupItem>
            <input type="radio" value="EveryDay" name="repeatPeriod" defaultChecked/> EveryDay
          </ListGroupItem>
          <ListGroupItem>
            <input type="radio" value="EveryWeek" name="repeatPeriod" /> EveryWeek
          </ListGroupItem>
          <ListGroupItem>
            <input type="radio" value="EveryMonth" name="repeatPeriod" />
            EveryMonth
          </ListGroupItem>
        </ListGroup>
        <DatePickerDayAndTime onChange={this.onTimeChange} />
        <Button style={{margin:10}} onClick={this.addRepeatingPeriod}>Add</Button>
      </div>
    );
  }
}
