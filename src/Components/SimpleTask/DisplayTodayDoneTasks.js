import React from "react";
import * as taskActions from '../../actions/TasksActions';
import { connect } from 'react-redux';

const DisplayTodayDoneTasks = props => <div>current tasks</div>;


const mapStateToProps = state => {
    return {
        tasks: state.TaskReducer.tasks,
    }
  }
  
  const mapDispatchToProps = dispatch => {
    return {
        addTask: (task) => dispatch(taskActions.addTask(task)),
        deleteTask: (id) => dispatch(taskActions.deleteTask(id)),
    }
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(DisplayTodayDoneTasks)
  