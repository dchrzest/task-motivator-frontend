import React, { Component } from "react";
import {
  Button,
  Form,
  FormGroup,
  Label,
  ListGroup,
  Input,
  FormText,
  ListGroupItem,
  Table
} from "reactstrap";
import RepeatingPeriod from "./RepeatingPeriod";
import SelectAddPoints from "./SelectAddPoints";
import * as taskActions from "../../actions/TasksActions";
import { connect } from "react-redux";
import axios from "axios";
import simpleTaskService from "../../Services/SimpleTaskService";


class EditSimpleTask extends React.Component {
  handleSubmit(event) {
    event.preventDefault();
    event.stopPropagation();

    const {
      Name,
      Points,
      Description,
      RepeatingPeriods,
      TaskType
    } = this.state;

    var task = {
      Points: Points,
      Name: Name,
      Description: Description,
      TaskType: TaskType,
      IsRepeating: RepeatingPeriods.length > 0,
      RepeatingPeriods: RepeatingPeriods
    };

    simpleTaskService.addTask(task, null);
    // debugger;
  }

  onTaskDataGet = (data) => {
   this.setState({
      isListHidden: data.isListHidden,
      TaskType:data.TaskType,
      Name: data.Name,
      Description: data.Description,
      Points: data.Points,
      RepeatingPeriods: data.RepeatingPeriods
    });
  };

  constructor(...args) {
    super(...args);
    simpleTaskService.getTask(this.props.id, this.onTaskDataGet);
 
  }

  handleInputChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleRepeatingPeriod = event => {
    console.log(event);
  };

  setGender(event) {
    console.log(event.target.value);
  }

  handlePoints = taskPoints => {
    this.setState({ Points: taskPoints });
  };

  setTaskType = event => {
    this.setState({
      TaskType: event.target.value
    });
    console.log(event.target.value);
  };


  addRepeatingPeriod = value => {
    console.log("Adding repeating period" + JSON.stringify(value));
    this.setState({
      RepeatingPeriods: [...this.state.RepeatingPeriods, value]
    });
  };

  componentDidUpdate() {
    console.log(
      "Repeating Periods" + JSON.stringify(this.state.RepeatingPeriods)
    );
  }
  render() {
    const { validated } = this.state;
    return (
      <div>
        <Form onSubmit={e => this.handleSubmit(e)}>
          <FormGroup>
            <Label>Name</Label>
            <Input
              name="Name"
              type="text"
              placeholder="Name"
              onChange={this.handleInputChange}
            />
          </FormGroup>

          <FormGroup>
            <Label>Description</Label>
            <Input
              name="Description"
              type="textarea"
              placeholder="Description"
              onChange={this.handleInputChange}
            />
          </FormGroup>
          <FormGroup>
            <Label>Points for task</Label>
            <SelectAddPoints onChange={this.handlePoints} />
          </FormGroup>

          <FormGroup>
            <Label>Task type</Label>
            <ListGroup onChange={this.setTaskType.bind(this)}>
          <ListGroupItem>
            <input type="radio" value="Deadline" name="tasktype" defaultChecked/> Deadline
          </ListGroupItem>
          <ListGroupItem>
            <input type="radio" value="Event" name="tasktype"  /> Event
          </ListGroupItem>
        </ListGroup>
          </FormGroup>

          <FormGroup check>
            <Label check>
              <Input
                type="checkbox"
                onChange={event => {
                  this.setState({ isListHidden: !event.target.checked });
                  this.handleInputChange(event);
                }}
              />{" "}
              Is task repeating?
            </Label>
          </FormGroup>
          {!this.state.isListHidden && (
            <div>
              <Table>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Time period</th>
                    <th>Starting time</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.RepeatingPeriods.map((period, index) => {
                    console.log(
                      "index: " + index + " " + JSON.stringify(period)
                    );
                    var taskDate = new Date(period.startTime);
                    return (
                      <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{period.repeatPeriod}</td>
                        <td>{taskDate.toLocaleString()}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
              <RepeatingPeriod onSubmit={this.addRepeatingPeriod} />
            </div>
          )}
          <Button variant="primary" type="submit">
            Edit task
          </Button>
        </Form>
      </div>
    );
  }
}

// EditSimpleTask.defaultProps = {
//   isListHidden: true,
//   TaskType: "Deadline",
//   Name: "",
//   Description: "",
//   Points: 0,
//   RepeatingPeriods: []
// };


const mapDispatchToProps = dispatch => {
  return {
    addTask: task => dispatch(taskActions.addTask(task))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(EditSimpleTask);
