import React from "react";
import {
  Form,
  Button,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

import { LinkContainer } from "react-router-bootstrap";
import { Link } from "react-router-dom";
import Routes from "../../Routes";

export default class StartPageNavbar extends React.Component {
  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">Motivator</NavbarBrand>
        </Navbar>
      </div>
    );
  }
}
