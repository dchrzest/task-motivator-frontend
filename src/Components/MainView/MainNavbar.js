import React from "react";
import {
  Form,
  Button,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

import { LinkContainer } from "react-router-bootstrap";
import { Link, withRouter } from "react-router-dom";
import Routes from "../../Routes";
import accountService from "../../Services/AccountService";

class MainNavbar extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  logout = () => {
    accountService.logout();
    this.props.history.push("/");
  };

  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">Motivator</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <LinkContainer to="/ManageTasks">
                <NavLink>Manage Tasks</NavLink>
              </LinkContainer>

              <LinkContainer to="/History">
                <NavLink>History</NavLink>
              </LinkContainer>
              <LinkContainer to="/Calendar">
                <NavLink>Calendar</NavLink>
              </LinkContainer>
            </Nav>
            <Form inline>
              <LinkContainer to="/CreateSimpleTask">
                <Button variant="outline-success">Add task</Button>
              </LinkContainer>
              <Button
                style={{ margin: 10 }}
                onClick={this.logout}
                variant="outline-danger"
              >
                Logout
              </Button>
            </Form>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
export default withRouter(MainNavbar);