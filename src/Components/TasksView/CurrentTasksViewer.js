import React, { Component } from "react";
import simpleTaskService from "../../Services/SimpleTaskService";
import { Table, Button, ButtonGroup, ButtonToolbar } from "reactstrap";
import simpleTaskActionsService from "../../Services/SimpleTaskActionsService";
import TimeLeft from "../Additionals/TimeLeft";
import {LinkContainer} from 'react-router-bootstrap'

export default class CurrentTasksViewer extends Component {
  constructor(props) {
    super();
    this.state = {
      tasks: []
    };
  }

  currentTasks = data => {
    console.log(data);
    this.setState({
      tasks: [...data]
    });
  };

  componentDidMount() {
    simpleTaskService.getCurrentTasks(this.currentTasks);
  }

  setTaskDone = task => {
    var sendingDate = {
      simpleTaskId: task.simpleTaskId,
      taskDateId: task.taskDateId,
      timeOfTaskDeadline: task.deadline
    };
    simpleTaskActionsService.setTaskDone(sendingDate, v => {
      console.log(v);
    });
  };

  render() {
   console.log(window.location.href);
  
   console.log(window.location.pathname);
    console.log(this.state.tasks);
    console.log(`${window.location.href}Edit/:Id`);
    return (
      <div>
        
        <h3>Current tasks</h3>
        <Table>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Points</th>
              <th>Time</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.state.tasks.map((task, index) => {
              var deadlineDate = new Date(task.deadline);
              console.log('${window.location.href}/Edit?Id=${task.simpleTaskId}');

              return (
                <tr key={index}>
                  <th>{index+1}</th>
                  <td>{task.name}</td>
                  <td>{task.points}</td>
                  <td><TimeLeft start= {Date.now()} end={deadlineDate}/></td>
                  <td>
                    <ButtonGroup size="sm">
                      <Button color="primary">Skip this time</Button>
                      <LinkContainer to="/EditSimpleTask">
                      <Button color="danger">
                         {/* <Link to={`${window.location.pathname}/Edit?Id=${task.simpleTaskId}`}>Edit</Link> */}
                         Edit
                      </Button>
                      </LinkContainer>
                      <Button
                        color="success"
                        onClick={() => this.setTaskDone(task)}
                      >
                        Done
                      </Button>
                    </ButtonGroup>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}
