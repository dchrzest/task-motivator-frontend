import React, { Component } from 'react'
import simpleTaskService from '../../Services/SimpleTaskService';
import {Table} from 'reactstrap'
import simpleTaskActionsService from '../../Services/SimpleTaskActionsService';
export default class TaskToCheckViewer extends Component {
    constructor(props) {
        super();
        this.state = {
          tasks: []
        };
      }
    
      currentTasks = (data) => {
        console.log(data);
        this.setState({
          tasks: [...data]
        });
      };
    
      componentDidMount() {
        simpleTaskService.getTasksToCheck(this.currentTasks);
      }
    
      setTaskDone = task => {
        var sendingDate = {
          simpleTaskId: task.simpleTaskId,
          taskDateId: task.taskDateId,
          timeOfTaskDeadline: task.deadline
        };
        simpleTaskActionsService.setTaskDone(sendingDate, v => {
          console.log(v);
        });
      };
    
      render() {
        console.log(this.state.tasks);
        return (
          <div>
            <h3>Current tasks</h3>
            <Table>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Points</th>
                  <th>Time</th>
                
                </tr>
              </thead>
              <tbody>
                {this.state.tasks.map((task, index) => {
                  var date = new Date(task.deadline);
                  return (
                    <tr key={index}>
                      <th>{index+1}</th>
                      <td>{task.name}</td>
                      <td>{task.points}</td>
                      <td>{date.toLocaleString()}</td>
                      
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </div>
        );
      }
}
