import { createStore, combineReducers, applyMiddleware } from "redux";
// import { createLogger, logger } from 'redux-logger';
import thunk from "redux-thunk";

import TasksReducer  from "./reducers/TasksReducer";

export default createStore(
  combineReducers({ TasksReducer }),
  {},
  applyMiddleware(thunk)
);
