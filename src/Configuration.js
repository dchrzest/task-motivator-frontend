//file for configuration keys and values

const configuration = {
    hostUrl: (window && window.location && window.location.hostname).indexOf('localhost') === -1 ? 'https://tutajstronanaserwerze' : 'http://localhost:44314',
    apiUrl: 'https:/localhost:44314/api/'
}

export default configuration;