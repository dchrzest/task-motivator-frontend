import React, { Component } from "react";
import simpleTaskService from "./Services/SimpleTaskService";
import TimeLeft from "./Components/Additionals/TimeLeft";
import {Table} from 'reactstrap'

export default class History extends Component {
    constructor(props) {
        super();
        this.state = {
          history: []
        };
      }
    
      currentTasks = (data) => {
        console.log(data);
        this.setState({
            history: [...data]
        });
      };
    
      componentDidMount() {
    console.log(window.location.href);

        simpleTaskService.getFullHistory(this.currentTasks);
      }

  render() {
    return (
      <div>
        <Table>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Points</th>
              <th>Time</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.state.history.map((task, index) => {
              var date = new Date(task.deadline);
              return (
                <tr key={index}>
                  <th>{index+1}</th>
                  <td>{task.name}</td>
                  <td>{task.points}</td>
                  <td>{date.toLocaleString()}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    )
  }
}

