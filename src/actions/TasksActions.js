import axios from 'axios';
import { ServerURL, tasksSubstring } from '../ServerURL';

export function addTask(task) {

    debugger;
    return function (dispatch) {
    debugger;

        return axios.post(ServerURL() + tasksSubstring(), task)
            .then((response) => {
            debugger;
            
                console.log("adding success ", response);
                dispatch({ type: "ADD_TASK", payload: task });
            })
            .catch(function (error) {
            debugger;

                console.log(error);
            });
    };
}

export function changeTask(id, task) {
    return function (dispatch) {
        return axios.put(ServerURL() + tasksSubstring(), task)
            .then((response) => {
                console.log("changing success ", response);
                dispatch({ type: "CHANGE_TASK", payload: task, Id: id });
            })
            .catch(function (error) {
                console.log("changing failed ", error);
                console.log(error);
            });
    };
}

export function deleteTask(id) {
    return function (dispatch) {
        return axios.delete(ServerURL() + tasksSubstring() + "/" + id)
            .then((response) => {
                console.log("Deleting person success");
                dispatch({ type: "DELETE_TASK", payload: id });
            })
            .catch(function (error) {
                console.log("Deleting person failed");
                console.log(error);
            });
    };
}

export function getTasks() {
    return function (dispatch) {
        return axios.get(ServerURL() + tasksSubstring())
            .then((response) => {
                console.log("getting success ", response);
                dispatch({ type: "ADD_TASK", payload: response.data });
            })
            .catch(function (error) {
                console.log("getting error");
                console.log(error);
            });
    };
}