export const ServerURL = () => {
  return "localhost:44314/api/";
};
export const tasksSubstring = () => {
  return "/SimpleTasks";
};
export const usersSubstring = () => {
  return "/Users";
};